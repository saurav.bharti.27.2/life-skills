## 1. Grit

### Question. Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

* Grit is passion and perseverance for long-term goals, sticking with the future, day in and day out, and Living life like a marathon not like a sprint.

* Sometimes, Grit is unrelated and irreversely related to measures of talent.

* Growth mindset - Belief that the ability to learn is not fixed, is the best way to build Grit.

## 2. Introduction to Growth Mindset

### Question. Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

There are two types of mindset.

1. Fixed Mindset

    * Some people believe that skills and intelligence are set and people either have them or not.
    * They believe that we are not in control of our abilities.
    * Skills are born not built.

2. Growth Mindset

    * People with a Growth mindset believe that Skills and intelligence are grown and developed.
    * They believe that we are in control of our abilities.
    * They believe practice, challenges, mistakes, and feedback are very important key Ingredients to growth.

## 3. Understanding Internal Locus of Control

### Question. What is the Internal Locus of Control? What is the key point in the video?

Internal Locus of Control is believing that we have control over our life and actions, not believing that what happens to us is the result of luck or fate, or is determined by people in authority.

        The video describes an experiment that Claudia M. Mueller conducted in 1998. She did an experiment where she gave a bunch of kids a few puzzles to solve. Irrespective of the result she told all the kids they did quite well, she told half of the kids that they are smart and gifted, and half that they are hardworking and focused.

                    A few days after she gave them puzzles of three categories, i.e. hard, medium, and easy. She noticed that the kids that she told were hardworking and focused and did much better than the kids who were told that they were smart and gifted.
                
The key point of the video is -

* To have an Internal locus of control and believe that they control their own fate and their actions only will determine their future.
* People with an internal locus of control take greater responsibility and accountability in their course of action.

## 4. How to build a Growth Mindset

### Question. What are the key points mentioned by speaker to build growth mindset?

A few key points mentioned by speaker to build a growth mindset are mentioned below - 

* Believe to figure it out and do it.
* Question the assumptions.
* Don't let your current knowledge and ability intervene for tomorrow.
* Design the curriculum, be open to new ideas, and realize that the world is changing.
* Honour the struggle. Don't hate the process. 
* Realizing that this difficulty will help us become more emphatic, is forging a stronger character inside us.

## 5. Mindset - A MountBlue Warrior Reference Manual

### Question. What are your ideas to take action and build Growth Mindset?

My Ideas to take action and build Growth Mindset are the following 

* Developing grit, with a growth mindset.
* Believing that my actions will decide my future.
* Having an Internal locus of control, taking greater responsibility and accountability for my actions.
* Having the spirit to take up any new challenge, figure it out, and doing it.
* Skill to question my own assumptions.

## Reference

* https://www.betterup.com/blog/locus-of-control
* https://www.youtube.com/watch?v=W-ONEAcBeTk
