## Active Listening

### What are the steps/strategies to practice Active Listening?
Active Listening involves listening attentively with all senses, understanding both verbal and non-verbal cues. Here are the steps to practice it effectively:
- Maintain focus on the speaker and the topic without getting distracted.
- Allow the speaker to finish before responding.
- Use encouraging phrases like "Go ahead" and “Interesting, tell me more" to keep the conversation flowing.
- Display appropriate body language such as nodding and maintaining eye contact.
- Paraphrase what the speaker said to ensure mutual understanding.
- Take notes if necessary to retain key points.

## Reflective Listening

### According to Fisher's model, what are the key principles of Reflective Listening?
Reflective Listening is about fully engaging with the speaker, then mirroring their message back for confirmation. Key principles from Fisher's model include:
- Prioritizing listening over speaking.
- Using paraphrasing to validate understanding.
- Recognizing and acknowledging the speaker's emotions alongside facts.
- Responding with empathy and acceptance.
- Ensuring responses are relevant to the conversation topic.
- Adopting the speaker's perspective while avoiding biases.

## Reflection

### What are the obstacles in your listening process?
In my listening process, I encounter several obstacles:
- Intrusive thoughts that disrupt focus.
- Environmental disturbances like noise or interruptions.
- Impatience, which may lead to premature responses or interruptions.
- Boredom when the subject matter lacks interest.
- Prejudices or biases that cloud understanding.

### What can you do to improve your listening?
To improve my listening skills, I can implement these strategies:
- Practice active engagement by giving undivided attention to the speaker.
- Employ techniques to minimize distractions and maintain focus.
- Demonstrate attentive body language to signal active listening.
- Strive to empathize and understand the speaker's perspective.
- Practice patience, allowing speakers to express themselves fully.

## Types of Communication

### When do you switch to Passive communication style in your day to day life?
Passive communication is a type of communication in which a person sets aside their desires and needs and instead focuses on the needs, desires, and opinions of others. In my day-to-day life, I tend to adopt passive communication when I prioritize pleasing others over expressing my own needs and opinions. This often occurs when I feel uncomfortable asserting myself or when interacting with individuals in positions of authority, leading me to neglect my own perspectives.

### When do you switch into Aggressive communication styles in your day to day life?
Aggressive communication involves forcefully expressing opinions, needs, and desires, sometimes at the expense of disrespecting others. I tend to resort to an aggressive communication style when I perceive my opinions and needs are disregarded by others. Additionally, during moments of intense emotion, particularly when upset, I may default to aggressive communication as a means of asserting my position.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
Passive-aggressive communication involves indirectly expressing aggression rather than openly addressing it. I find myself adopting this style when I'm unable or unwilling to confront a situation directly. It may manifest through sarcasm, gossip, or giving someone the silent treatment, particularly when I feel unable to express my feelings openly or fear confrontation.

### How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)
Assertive communication involves expressing opinions and needs clearly without being aggressive or disrespectful. Here are steps I can apply in my own life to cultivate assertive communication:
- Clearly articulate my feelings and emotions to facilitate open communication.
- Precisely identify my needs and opinions to ensure clarity in communication.
- Practice assertive communication gradually, starting with simpler situations and gradually progressing to more complex ones.
- Be mindful of my body language to ensure it complements assertive communication and does not convey aggression.
- Initiate the practice of assertive communication as soon as possible to nurture healthier relationships and interactions.
- Exercise caution in communication to avoid resorting to aggressive or passive styles, prioritizing assertiveness for effective dialogue and understanding.

## References
* https://en.wikipedia.org/wiki/Reflective_listening
* https://www.youtube.com/watch?v=rzsVh8YwZEQ
* https://www.youtube.com/watch?v=mixJEpGTvGw
* https://www.youtube.com/watch?v=vlwmfiCb-vc
