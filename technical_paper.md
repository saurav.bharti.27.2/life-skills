
# SQL

Structured query language (SQL) is a programming language for storing and processing information in a relational database. A relational database stores information in tabular form, with rows and columns representing different data attributes and the various relationships between the data values. We can use SQL statements to store, update, remove, search, and retrieve information from the database.

## SQL Table

A SQL table is the basic element of a relational database. The SQL database table consists of rows and columns. Users can create relationships between multiple database tables to optimize data storage space.

Table - Subjects

|   Id | Student | subject |
| ------:  | :-----: | :----|
|1 | abc | Maths|
|2 | xyz | Science|
|3 | mno | Chemistry| 

Table - Marks

| Id | Marks |
| -----: | :----- | 
| 1 | 86|
|2 | 91 |
|3 | 73 |


Here, both tables can be related by their Id's. Using SQL, we can join both tables using the SQL 'Join' clause.
Like,

```sql
SELECT * FROM Subjects 
INNER JOIN
Marks ON Subjects.Id = Marks.Id;
``` 

The outcome of this is - 

| Id | Student | subject   | Marks |
|----:|:---------:|:-----------:|:-------|
| 1  | abc     | Maths     | 86    |
| 2  | xyz     | Science   | 91    |
| 3  | mno     | Chemistry | 73    |

#### The Ability to do these operations in a database makes SQL special. Now, Let's learn more about the basics of SQL. 

## SQL Datatypes

The listed SQL-datatypes are the most used datatypes, that we use for table creation.

| Data Type  | Description                        |
|------------|------------------------------------|
| INT        | Integer (whole number)             |
| VARCHAR(n) | Variable-length character string   |
| CHAR(n)    | Fixed-length character string      |
| DATE       | Date in YYYY-MM-DD format          |
| TIME       | Time in HH:MM:SS format            |
| DATETIME   | Date and time in YYYY-MM-DD HH:MM:SS format |
| DECIMAL(p, s) | Exact numeric with specified precision and scale |
| FLOAT(p)   | Floating-point number with specified precision |
| BOOLEAN    | True or false value                |
| BLOB       | Binary large object                |


## SQL Commands

1. #### CREATE   
   It creates a new table in the database. We can also declare the columns that we want here.

   ```sql
   CREATE TABLE practice 
   ( id INT PRIMARY KEY, topic VARCHAR(50), duration TIME );
   ```

2.  #### INSERT  
    It is used to insert values in the row of tables. 

    ```sql
    INSERT INTO practice
    VALUES (1, "JAVASCRIPT", "12:00");
    ```

    The table will look like this  
    |id	|topic|	duration|
    |:--- |:----: | ---: |
    |1	|JAVASCRIPT|	12:00|

3. #### ALTER  
    Used to modify existing database Table.

    ```sql
        ALTER TABLE practice
        ADD student varchar(100);
    ```

    This will add a new column in our table "practice".

4. #### UPDATE  
    Used to update the table's row data.

    ```sql
        UPDATE practice
        set 
        duration = "6:00"
        where
        id=1;
    ```

5. #### DROP  
    Used to drop the entire table.

    ```sql
        DROP TABLE practice;
    ```

6. #### DELETE  
    Used to delete rows from a table.

    ```sql
        DELETE FROM practice
        where
        id = 1;
    ```


## SQL Queries 

1. #### SELECT   
    The SELECT Statement in SQL is used to retrieve or fetch tables from a database. 

    ```sql
    SELECT * FROM practice;
    ```
    It will select all the columns in the table.

    ```sql
    SELECT practice.topic FROM practice; 
    ```
    It will select only the mentioned column.

2. #### WHERE
    We can add this where clause to filter our queries.

    ```sql
    SELECT * FROM practice
    WHERE 
    practice.topic = "JAVASCRIPT" 
    ```

    It will only select the rows with Javascript as a topic.

3. #### ORDER BY  
    It will sort the query based on the mentioned criteria. By default, it sorts in ascending order.

    ```sql
    SELECT * FROM practice
    ORDER BY practice.duration DESC
    ```

    Here, I ordered the rows in descending order by using 'DESC', the same can be done for ascending order by using 'ASC'.

4. #### GROUP BY
    It is used to group the table by a specific column. If a particular column has the same values in different rows then it will arrange these rows in a group. 

    ```sql
    SELECT * FROM practice
    GROUP BY practice.topic
    ```

    Here, I'm grouping all the rows by their topic. So, All the rows that have "JAVASCRIPT" as a topic will be in one group. Each topic will form their own group.



## SQL JOIN  

SQL Join statement is used to combine data or rows from two or more tables based on a common field between them. Different types of Joins are as follows: 

* INNER JOIN
* LEFT JOIN
* RIGHT JOIN


1. #### INNER JOIN

    Tables that share information about a single entity that is unique for each row inside the Table is called as primary key. Inner join selects both table rows  based on the primary key, as long as none of them are NULL on the condition specified.

    ```sql
    SELECT * FROM table1
    INNER JOIN
    table2
    on table1.primary_key = table2.primary_key
    ``` 

2. #### LEFT JOIN

    The LEFT JOIN keyword in SQL returns all matching rows that are present in the left table but not in the right table. Even if some keys are missing or NULL on the right table it will still 
    show the output prioritizing the left table.

    Table Students

    | StudentId | Name  |
    |:-------- | -----: |
    | 1         | John     |
    | 2         | Alice    |
    | 3         | Bob      |
    | 4         | Sarah    |



    Table Grades

    | StudentId | Grade    |
    | :--------- | --------: |
    | 1         | A        |
    | 3         | B        |
    | 5         | C        |
    | 6         | D        |

    So, If we left join table Students to table Grades, we will still get the rows that don't have any presence on the Table Grades. Unlike, INNER JOIN.

    ```sql
    SELECT Students.StudentId, Students.Name, Grades.Grade
    FROM Students
    LEFT JOIN Grades ON Students.StudentId = Grades.StudentId;
    ```

    The table formed is prioritizing the left Table i.e. Students
   
    | StudentId | Name     | Grade    |
    |-----:| :------:| :----- |
    | 1         | John     | A        |
    | 2         | Alice    | NULL     |
    | 3         | Bob      | B        |
    | 4         | Sarah    | NULL     |


3. #### RIGHT JOIN
    
    Does, the same as LEFT JOIN but for the right table. So, for the same table Students and Grades. 

    | StudentId | Name     | Grade    |
    | --------- | -------- | -------- |
    | 1         | John     | A        |
    | 3         | Bob      | B        |
    | 5         | NULL     | C        |
    | 6         | NULL     | D        |



    Here, We prioritized the right table Grades, even if few students were not present but we still gave the output for all the Grades.


## SQL AGGREGATIONS

* SQL aggregation function is used to perform the calculations on multiple rows of a single column of a table. It returns a single value.
* It is also used to summarize the data.

    List of some common aggregate functions.

    | Function     | Description                                                                                                                                                           |
    | ------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | COUNT()     | A common function used to count the number of rows in the group if no column name is specified. Otherwise, count the number of rows in the group with non-NULL values in the specified column. |
    | MIN(column)  | Finds the smallest numerical value in the specified column for all rows in the group.                                                                                 |
    | MAX(column)  | Finds the largest numerical value in the specified column for all rows in the group.                                                                                  |
    | AVG(column)  | Finds the average numerical value in the specified column for all rows in the group.                                                                                  |
    | SUM(column)  | Finds the sum of all numerical values in the specified column for the rows in the group.    


    | StudentID | Name   | Score |
    | --------- | ------ | ----- |
    | 1         | John   | 85    |
    | 2         | Alice  | 90    |
    | 3         | Bob    | 75    |
    | 4         | Sarah  | 95    |
    | 5         | Emily  | 80    |

    ### Given, Below are illustrations for each type of aggregate function.

1. ### COUNT 

    Table- Students


    ```sql
    SELECT COUNT(Students.Name) as TotalStudents FROM Students;
    ```

    | TotalStudents |
    | ------------- |
    | 5             |

2. ### MIN

    ```sql
    SELECT MIN(Score) AS MinScore FROM Students;
    ```

    | MinScore |
    | -------- |
    | 75       |

3. ### MAX

    ```sql
    SELECT MAX(Score) AS MaxScore FROM Students;
    ```

    | MaxScore |
    | -------- |
    | 95       |

4. ### AVG

    ```sql
    SELECT AVG(Score) AS AvgScore FROM Students;
    ```

    | AvgScore |
    | -------- |
    | 85       |

5. ### SUM

    ```sql
    SELECT SUM(Score) AS TotalScore FROM Students;
    ```

    | TotalScore |
    | ---------- |
    | 425        |


# Reference

* https://sqlbolt.com/lesson/introduction
* https://www.geeksforgeeks.org/sql-join-set-1-inner-left-right-and-full-joins/
* https://en.wikipedia.org/wiki/SQL

