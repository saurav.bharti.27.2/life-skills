## 1. Tiny Habits - BJ Fogg

### In this video, what was the most interesting story or idea for you?

There were a few interesting stories, that were present in the video

* The author bought a weighting scale that can automatically tweet out the body weight, he used it for 1 year. It didn't have any impact, but it created the habit of getting up on scale every day. And after that, he put together these tiny habits and finally achieved his desired weight.

* Developing tiny habits, like flossing just one tooth after brushing.

* He gave the example of Pumpkin seedings, where a tiny seed in the right spot can give a huge fruit with time.

## 2. Tiny Habits by BJ Fogg - Core Message

### How can you use B = MAP to make making new habits easier? What are M, A and P.

According to BJ Fogg, B = MAP means,

     Human Behavior = Motivation, Ability and Prompt 
    
We can use B = MAP to make new habits easier in three steps -

* Motivation - Shrinking every new habit to the tinest possible habits version, so that little motivation is needed.

* Action Prompt - Using the completion of one behavior to trigger action for another behavior.

* Shine - Learing celebrating after completion of tiny wins.

### Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

Shine is the most important step in the process of habit development. Even when the task is small still it increases confidence and motivation to start the task again. Its ability to increase the motivation is unique.  

When you celebrate your good habit, no matter how tiny, you're deepening your connection to that habit, and that in turn can make you feel like you have more control over your life and your world, which, as Fogg says, can translate to how you treat other people around you.

## 3. 1% Better Every Day Video

### In this video, what was the most interesting story or idea for you?

The most important idea of the entire video is "Aggregation of marginal gain".

BJ Frogg narrates the story of British Cycling, where while being a very reputed and rich company, their performance was very poor in TOUR DE FRANCE.
So, they brought Dave Brailsford, to improve their record.

He implemented very minute small changes on the cycling team like - 
* Lighter tires.
* Better seats
* Rider wearing Bio-Feedback sensors.

Also, he made some unusual changes, like -
* Splitting massage gel, for better muscle recovery.
* Taking different pillows to the restaurant.
* Washing hands to avoid infections.

After, doing minute changes with time he achieved excellent results. British Cycling won 4 out of the next 5 tours, they won 70% of cycling gold medals in the Olympics.

## 4. Book Summary of Atomic Habits

### What is the book's perspective about Identity?

* Identity change is the north star of habit change.
* The ultimate form of intrinsic motivation is when a habit becomes part of our identity.
* Solving problems in terms of outcome and result, we only solve them temporarily but to solve the problem in the longer term system-wise, we have to change our identity.

### Write about the book's perspective on how to make a habit easier to do?

Books state these 4 principles to make a habit easy to do - 
1. Make it obvious
2. Make it attractive
3. Make it easy.
4. Make it satisfying. (Rewarding)

### Write about the book's perspective on how to make a habit harder to do?

1. Decrease motivation -
    * If the motivation to perform a habit is low, we are less likely to engage in it. 
2. Decrease Ability -
    * This can be done by adding obstacles, increasing the effort required, or introducing barriers that make it more challenging to engage in the behavior.
3. Remove Triggers -
    * By removing or minimizing triggers associated with the habit, it becomes harder to engage in the behavior.

## 5. Reflection:

### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

The one habit that I would like to do more of is starting to do regular workouts.

The steps that I will take to make the cue obvious are:-
* Breaking it into tiny habits, like starting with 5 pushups in a day, right before when I go to bath.
* Celebrating after fulfilling the task, even though, the task is small still increases confidence and motivation to start the task again.
* Relating it with other habits, so that It will get triggered easily.

### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible the process unattractive or hard or the response unsatisfying?

The one habit that I would like to eliminate is the use of social media.

The steps to make the process cue invisible or process unattractive are -   

* Disable push notifications: Turn off notifications for social media apps on your phone to reduce the constant reminders and urges to check them.
* Track the usage: Using apps or features that track our social media usage and provide insights into how much time we're spending on each platform.
* Reward alternative behaviors: Replace social media use with more productive or fulfilling activities, and reward yourself for engaging in these behaviors instead.

## References

* https://www.youtube.com/watch?v=S_8e-6ZHKLs
* https://behaviordesign.stanford.edu/resources/fogg-behavior-model
* https://www.youtube.com/watch?v=mNeXuCYiE0U
