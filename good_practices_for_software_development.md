## Good Practices for Software Development

### Question 1: Which point(s) were new to you?


- Taking frequent feedback helps in understanding requirements thoroughly and ensures alignment with stakeholders.
- Communicating effectively about missed deadlines and setting new expectations is crucial for project transparency and stakeholder trust.
- Clear articulation of problems and attempted solutions fosters efficient collaboration and accelerates issue resolution.
- Understanding team dynamics enhances communication effectiveness, leading to smoother workflows and improved productivity.
- Consolidating queries into a single message streamlines communication, reducing clutter and facilitating better responses.
- Upholding work ethics, even in remote environments, ensures professionalism and maintains team cohesion and productivity.

### Question 2: Which area do you think you need to improve on? What are your ideas to make progress in that area?


1. Establishing a fixed schedule for leisure activities helps in maintaining work-life balance and prevents overworking.
2. Minimizing distractions during work hours fosters focus and productivity, enabling better adherence to work-play boundaries.
3. Incorporating regular exercise and a balanced diet boosts energy levels and mental clarity, supporting sustained focus and performance in work tasks.
4. Following the "Work when you work, play when you play" motto more strictly.

### References

* https://www.thinkful.com/blog/software-engineering-best-practices/
* https://github.com/mountblue/life-skills-track/blob/main/good-practices-for-software-development.md
