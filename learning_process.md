
### 1. How to Learn Faster with the Feynman Technique

Named after the great Noble winning scientist, teacher and explainer Richard Feynman. The technique states "If you understand something well, try to explain it simply."

### 2. In this video, what was the most interesting story or idea for you?

The most interesting story for me was how the Scientist and Inventor Thomas Edison would sit in his chair with a ball bearing in his hand, He would relax away, thinking about the problem loosely that he was trying to solve about his invention.   
Just as he'd fall asleep, the ball bearing would fall from his hands, and he'd be woken up. Then he'll take those ideas from diffuse mode into focus mode. He'd use them to refine and finish his inventions.

### 3. What are active and diffused modes of thinking?

* Active mode
    1. The active mode of thinking is like dealing with the problem in a way that we are familiar with.
    2. There will be fewer new ideas and more concentrated and conscious thinking in a way that we are already aware of
    3. This method stresses practicing and achieving perfection through repetition.

* Diffused mode
    1. When our minds are free to wander, we shift into a diffuse mode of thinking, although diffuse thinking comes in the guise of a break from focus, our minds are still working.
    2. Diffuse thinking takes on a more creative and broad-minded approach
    3. Diffuse mode can be a very brief phenomenon, such as when we briefly stare into the distance before returning to work.

### 4. According to the video, what are the steps to take when approaching a new topic? Only mention the points.

1. Deconstruct the skills, break the skills into simpler parts  

2. Learn enough to self-correct - use books, courses, etc. But don't procrastinate.

3. Remove any distractions from practice. 

4. Practice for at least 20 hours while learning anything new.

### 5. What are some of the actions you can take going forward to improve your learning process?

1. Keeping a steady balance between focused and diffused thinking.

2. Explain the concepts that I learned to myself or anybody else.

3. Removing the distractions that might stop me from practicing.

4. Learning enough to self-correct myself.


## References

* https://www.youtube.com/watch?v=O96fE1E-rf8
* https://www.youtube.com/watch?v=5MgBikgcWnY
* https://fs.blog/focused-diffuse-thinking/



