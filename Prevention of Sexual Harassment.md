
## Question. What kinds of behavior cause sexual harassment?

#### Any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment.

Sexual Harassment is of 3 forms.  
1. Verbal
2. Visual
3. Physical

#### Verbal  

* Comment about the clothing, person's body, and sexual or gender-based jokes.
* Requesting sexual favors or repeatedly asking a person out.
* Threats, spreading rumors about some people's personal or sexual lives.
* Using foul and obscene language.


#### Visual

* Obscene posters.
* Sexual drawing/pictures.
* Vulgar screen saver, cartoons, emails, text.

#### Physical

* Inappropriate touching such as hugging, patting, stroking, and rubbing.
* Sexual assaults.
* Impeding or blocking movement.
* Sexual gestures, leering, and staring. 

#### Categories of sexual harassment -  

1. <b>QUID PRO QUO</b> - "this for that", using job rewards like promotions or cash rewards or punishments to coerce an employee into sexual favors. 
2. <b>Hostile Work Environment</b> -  Where an employee's behavior interferes with the work performance of others.


## Question. What would you do in case you face or witness any incident or repeated incidents of such behavior?

#### 1. Confront 
* Tell the person to stop, It has been reported that up to 90% of the time it stops the harassment.  

#### 2. Distract 
* Indirectly diffuse the situation by interrupting the harasser and the victim. Like, Ask the harasser for information such as the time, directions, upcoming meeting details, or any random information intended to redirect the harasser from continuing to target the victim.  

#### 3. Delegate
* Ask someone for help with intervening in harassment. Look for someone close in proximity and willing to help. Supervisors or leaders are ideal delegates, following employee grievance procedures, and talking to someone in Human Resources.

#### 4.  Document
* Record or take notes of the harassment if someone is already helping the targeted person and if your safety is ensured. Always ask the person who was harassed if they want your recording or notes, and don’t use them without their permission, especially posting online.

## References

* https://www.ncbi.nlm.nih.gov/books/NBK587339/
* https://www.youtube.com/watch?v=Ue3BTGW3uRQ
* https://legalvoice.org/sexual-harassment-at-work/




